#!/bin/sh

#
# De-provision NeuroDebian Bot in Podman pod
# (inner part executed in a throwaway container)
#
# Author: Vasyl Gello <vasek.gello@gmail.com>
#

# Start of script

# Check the presence of Salsa token

if [ -z "$SALSATOKEN" ]; then
	cat >&2 <<.p
ERROR: Debian Salsa token is not present in SALSATOKEN
environment variable!
.p

	exit 1
fi

# Check the presence of SSHPUBKEY

if [ -z "$SSHPUBKEY" ]; then
	cat >&2 <<.p
ERROR: SSH public key is not present in SSHPUBKEY
environment variable!
.p

	exit 1
fi

# Check the presence of GPGPUBKEY

if [ -z "$GPGPUBKEY" ]; then
	cat >&2 <<.p
ERROR: GPG public key is not present in GPGPUBKEY
environment variable!
.p

	exit 1
fi

# Install the required dependencies

export DEBIAN_FRONTEND=noninteractive

apt-get update 1>/dev/null 2>&1 &&
	apt-get install -yq \
		curl \
		jq \
		sed \
		1>/dev/null 2>&1
RET=$?
if [ $RET -ne 0 ]; then
	echo "ERROR: Installing dependencies failed!" >&2
	exit $RET
fi

# Select and delete SSH public key

export SSHPUBKEY_NORMALIZED="$(
	cut -d' ' -f1,2 0<<.p
$SSHPUBKEY
.p
)"

SSH_KEY_ID="$(
	curl -s \
		"https://salsa.debian.org/api/v4/user/keys" \
		--header @/dev/fd/4 4<<.p |
\
PRIVATE-TOKEN: $SALSATOKEN
.p
		jq -e '.[] | select(.key | split(" ") | "\(.[0]) \(.[1])" == env.SSHPUBKEY_NORMALIZED ) | .id'
)"
RET=$?
if [ $RET -ne 0 ] || [ "$SSH_KEY_ID" = "null" ]; then
	echo "ERROR: Finding SSH key on Debian Salsa failed!" >&2
	exit $RET
fi

curl -s \
	-X DELETE \
	"https://salsa.debian.org/api/v4/user/keys/$SSH_KEY_ID" \
	--header @/dev/fd/4 4<<.p 1>/dev/null 2>&1
PRIVATE-TOKEN: $SALSATOKEN
.p
RET=$?
if [ $RET -ne 0 ]; then
	echo "ERROR: Deleting SSH key from Debian Salsa failed!" >&2
	exit $RET
fi

# Select and delete GPG public key

export GPGPUBKEY
export GPGPUBKEY_NORMALIZED="$(jq -n 'env.GPGPUBKEY' |
	sed 's/^.*----- BEGIN/----- BEGIN/;s/BLOCK -----.*$/BLOCK -----/' |
	jq -r '.')"

GPG_KEY_ID="$(
	curl -s \
		"https://salsa.debian.org/api/v4/user/gpg_keys" \
		--header @/dev/fd/4 4<<.p |
\
PRIVATE-TOKEN: $SALSATOKEN
.p
		jq -e ".[] | select(.key == env.GPGPUBKEY_NORMALIZED) | .id"
)"
RET=$?
if [ $RET -ne 0 ] || [ "$GPG_KEY_ID" = "null" ]; then
	echo "ERROR: Finding GPG key on Debian Salsa failed!" >&2
	exit $RET
fi

curl -s \
	-X DELETE \
	"https://salsa.debian.org/api/v4/user/gpg_keys/$GPG_KEY_ID" \
	--header @/dev/fd/4 4<<.p 1>/dev/null 2>&1
PRIVATE-TOKEN: $SALSATOKEN
.p
RET=$?
if [ $RET -ne 0 ]; then
	echo "ERROR: Deleting GPG key from Debian Salsa failed!" >&2
	exit $RET
fi

cat 0<<.p >&2
NOTE: Debian Salsa token can not be deleted via GitLab API.

If that token was created specifically for the bot instance no longer in use,
please revoke it via web browser:

  - visit Debian Salsa ( https://salsa.debian.org ),
  - open "Preferences - Access Tokens"
  - Delete or revoke tokens that are no longer needed

.p
