#!/bin/sh

#
# Console script for NeuroDebian Bot
#
# Author: Vasyl Gello <vasek.gello@gmail.com>
#

# Default values

DEBEMAIL_DEFAULT='NeuroDebian Bot <nd-bot@neuro.debian.net>'
PODMAN_DEFAULT='podman'
PODMAN_SOCKET_VOLUME_DEFAULT='dangerous-podman-socket'

# Provisioning

deprovision() (
	# Exit if no secrets starting with POD_NAME exist

	if ! "$PODMAN" secret ls --format='{{range .}}{{.Name}}\n{{end -}}' -n |
		grep -q "^$POD_NAME-"; then
		echo "Secrets for $POD_NAME are already de-provisioned!" >&2
		return 0
	fi

	# Populate secret envvars by stripping BOT_NAME and _ and converting
	# to uppercase

	SECRETS=""
	for i in $("$PODMAN" secret ls --format='{{range .}}{{.Name}}\n{{end -}}' -f "name=^$POD_NAME-" -n); do
		SECRETENV="$(echo "$i" | sed "s/^$POD_NAME-//;s/-//g" | tr '[:lower:]' '[:upper:]')"
		SECRETS="$SECRETS --secret $i,type=env,target=$SECRETENV"
	done

	# Perform the necessary deletions in a throwaway
	# container so we don't care about installing dependencies like
	# curl, gnupg, jq etc.

	if ! "$PODMAN" run \
		--rm \
		$SECRETS \
		--mount type=bind,src="$SCRIPTDIR/scripts/deprovision-inner",dst="/provision/deprovision-inner",ro=true \
		debian:sid-slim \
		/bin/sh /provision/deprovision-inner; then
		echo "ERROR: De-registration of secrets on Debian Salsa failed!" >&2
		return 1
	fi

	"$PODMAN" secret rm $("$PODMAN" secret ls --format='{{range .}}{{.Name}}\n{{end -}}' -f "name=^$POD_NAME-" -n)
)

provision() (
	# Check if any secret starting with POD_NAME exists

	if "$PODMAN" secret ls --format='{{range .}}{{.Name}}\n{{end -}}' -n |
		grep -q "^$POD_NAME-"; then
		echo "Secrets for $POD_NAME are already provisioned!" >&2
		echo "Aborting!" >&2
		return 1
	fi

	# Perform the necessary validation and generation in a throwaway
	# container so we don't care about installing dependencies like
	# curl, gnupg, jq etc.

	"$PODMAN" run \
		--rm \
		--env DEBEMAIL \
		--env DEBIAN_SALSA \
		--env POD_NAME \
		--mount type=bind,src="$SCRIPTDIR/scripts/provision-inner",dst="/provision/provision-inner",ro=true \
		--mount type=tmpfs,dst="/root" \
		debian:sid-slim \
		/bin/sh /provision/provision-inner |
		while IFS='|' read SECRETNAME SECRETTYPE SECRET; do
			if [ -z "$SECRETNAME" ]; then
				break
			fi
			echo "Provisioning secret '$SECRETNAME' ..." >&2
			case "$SECRETTYPE" in
			"k8s")
				"$PODMAN" secret create "$SECRETNAME" - \
					0<<.p 1>/dev/null
apiVersion: v1
kind: Secret
metadata:
  name: $SECRETNAME
type: Opaque
data:
  value: $SECRET
.p
				;;
			"raw")
				base64 -d 0<<.q | "$PODMAN" secret create "$SECRETNAME" - 1>/dev/null
$SECRET
.q
				;;
			esac
		done

	# Inspect secrets starting with POD_NAME again

	if ! "$PODMAN" secret ls --format='{{range .}}{{.Name}}\n{{end -}}' -n |
		grep -q "^$POD_NAME-"; then
		echo "ERROR: Provisioning failed!" >&2
		return 1
	fi

	echo "[+] Pod '$POD_NAME' provisioned!" >&2

	return 0
)

# Create pod

create() (
	# Build container images based on expanded configuration

	while read _DUMMY _IMAGE _DIR _TAG; do
		[ "$_DUMMY" != '-' ] && continue

		if [ ! -f "$SCRIPTDIR/deployment/$_DIR/Containerfile" ]; then
			echo "ERROR: Configuration error - '$_DIR' does not exist!" >&2
			return 1
		fi

		"$PODMAN" build \
			-t "$_IMAGE" \
			-f "$SCRIPTDIR/deployment/$_DIR/Containerfile" \
			--build-arg TAG="$_TAG" \
			--build-arg POD_NAME="$POD_NAME" || return 1
	done 0<<.p
$("$PODMAN" run \
	--rm \
	--network none \
	-v "$SCRIPTDIR/deployment/build-spec.yaml:/build-spec.yaml:ro" \
	-v "$SCRIPTDIR/deployment/control/helpers.star:/helpers.star:ro" \
	-v "$SCRIPTDIR/deployment/control/bot-config-schema.yaml:/bot-config-schema.yaml:ro" \
	-v "$SCRIPTDIR/deployment/control/bot-config.yaml:/bot-config.yaml:ro" \
	docker.io/gerritk/ytt \
	-f /build-spec.yaml \
	-f /helpers.star \
	-f /bot-config-schema.yaml \
	--data-values-file /bot-config.yaml \
	--data-value botConfig.botName="$POD_NAME")
.p

	# Create the pod

	"$PODMAN" run \
		--rm \
		--network none \
		-v "$SCRIPTDIR/deployment/pod-spec.yaml:/pod-spec.yaml:ro" \
		-v "$SCRIPTDIR/deployment/control/helpers.star:/helpers.star:ro" \
		-v "$SCRIPTDIR/deployment/control/bot-config-schema.yaml:/bot-config-schema.yaml:ro" \
		-v "$SCRIPTDIR/deployment/control/bot-config.yaml:/bot-config.yaml:ro" \
		docker.io/gerritk/ytt \
		-f /pod-spec.yaml \
		-f /helpers.star \
		-f /bot-config-schema.yaml \
		--data-values-file /bot-config.yaml \
		--data-value botConfig.botName="$POD_NAME" |
	"$PODMAN" kube play --network none -
)

# Usage

usage() {
	cat >&2 <<.p
Usage:
    nd-bot-cli <action> <pod-name>

where:
    action       - Required action to perform. Can be one of:

      provision  - Generate necessary SSH and GPG keys and
                   register them with Debian Salsa. Requires the
                   following environment variables to run:
                   - DEBIAN_SALSA
                   - DEBEMAIL
                   If the script is invoked interactively, they
                   will be asked from the user.

      create     - Create NeuroDebian Bot instance.
                   Requires the SSH and GPG keys provisioned
                   first. Requires the leaked Podman socket
                   volume (see README.md) specified by
                   'PODMAN_SOCKET_VOLUME' environment variable.

      update     - Update NeuroDebian Bot instance.
                   Requires the bot instance previously created.

      start,
      stop       - Start or stop NeuroDebian Bot instance.

      destroy    - Delete the bot instance and all temporary containers.
                   Provisioned SSH and GPG keys are not deleted

      deprovision
                 - De-register all generated SSH and GPG keys from
                   Debian Salsa.

    pod-name     - Required name of pod to operate on.

Environment variables affecting the script:

    DEBEMAIL     - Name and email for authoring commits.
                   Default is '$DEBEMAIL_DEFAULT'

    DEBIAN_SALSA - Personal Access Token (PAT) to authenticate to
                   Debian Salsa (https://salsa.debian.org).

    PODMAN       - Path to podman binary. May be useful to
                   interact with custom podman instances.
                   Default is '$PODMAN_DEFAULT'

    PODMAN_SOCKET_VOLUME
                 - Name of leaked Podman socket (see README.md)
                   Default is '$PODMAN_SOCKET_VOLUME_DEFAULT'
.p
}

# Parse parameters and print usage

ACTION="$1"
POD_NAME="$2"

if [ -z "$ACTION" ]; then
	echo "ERROR: Action can not be empty!" >&2
	usage
	exit 1
fi

[ -z "$PODMAN" ] && PODMAN="$PODMAN_DEFAULT"

if test -z "$POD_NAME"; then
	echo "ERROR: Name of the pod can not be empty!" >&2
	usage
	exit 1
fi

export POD_NAME PODMAN

# Identify directory where script resides

SCRIPTDIR="$(readlink -f "$0")"
SCRIPTDIR="$(dirname "$SCRIPTDIR")"

# Check if podman is working

echo "[?] Check if podman is working ..."

if ! "$PODMAN" --version 1>/dev/null; then
	echo "ERROR: podman is not working!" >&2
	exit 1
fi

echo "[+] podman is working ..." >&2

case "$ACTION" in
provision)
	if test -z "$DEBIAN_SALSA"; then
		cat <<.p
Please log into Debian Salsa
(https://salsa.debian.org/-/profile/personal_access_tokens)
and create a Personal Access Token with 'api' scope.
.p

		if test -t 0; then
			read -p "Enter Debian Salsa token: " DEBIAN_SALSA
		fi
		if test -z "$DEBIAN_SALSA"; then
			echo "ERROR: Debian Salsa token cannot be empty!" >&2
			exit 1
		fi
	fi

	if test -z "$DEBEMAIL"; then
		if test -t 0; then
			read -p "Enter Debian name and email: " DEBEMAIL
		fi
		if test -z "$DEBEMAIL"; then
			DEBEMAIL="$DEBEMAIL_DEFAULT"
		fi
	fi
	export DEBIAN_SALSA DEBEMAIL
	provision
	;;
deprovision)
	deprovision
	;;
create)
	if ! "$PODMAN" volume exists "$PODMAN_SOCKET_VOLUME"; then
		echo "ERROR: Podman docket volume '$PODMAN_SOCKET_VOLUME' does not exist!" >&2
		exit 1
	fi
	export PODMAN_SOCKET_VOLUME
	create
	;;
*)
	echo "ERROR: Command not recognized '$ACTION'" >&2
	usage
	exit 1
	;;
esac

exit 0
