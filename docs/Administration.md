Administration of the NeuroDebian Bot
-

System Requirements
--

1. Bare-metal, virtual or cloud Linux machine capable of running Podman with minimum 4 CPU cores and 16GB RAM
2. Account on [Debian Salsa](https://salsa.debian.org)
3. GPG key authorized on Debian dak to upload packages (needed only if uploading to Debian official archive is required).

First-time deployment of NeuroDebian Bot instance
--

1. Deploy rootless Podman stack with rootless runsc

   On Debian GNU/Linux starting with bullseye (oldstable):

   ```
   su - root
   apt-get update
   apt-get install podman runsc
   ```

   or consult [this guide](https://podman.io/docs/installation).

   * Ensure UID/GID mappings work and unprivileged user namespaces are enabled on the host:

   ```
   cat /etc/subuid
   unshare -Ur id
   ```

   Consult [this guide](https://docs.sylabs.io/guides/latest/admin-guide/user_namespace.html) on how to enable unprivileged user namespaces if this step does not work out.

   * If runsc was not installed via package manager, install [runsc](https://gvisor.dev/docs/user_guide/install/#latest-release) manually.

   * As root, copy following script to `/usr/local/bin/runsc-rootless`:

     ```
     #!/bin/sh

     runsc --TESTONLY-unsafe-nonroot "$@"
     ```

     and make it executable:

     ```
     chown root:root /usr/local/bin/runsc-rootless
     chmod 0755 /usr/local/bin/runsc-rootless
     ```

   * Append the following snippet to `/etc/containers/containers.conf` just after `[engine.runtimes]` string to let Podman recognize the new runtime:

     ```
     runsc-rootless = [
        /usr/local/bin/runsc-rootless
     ] 
     ```

   * Verify runsc-rootless can be invoked py Podman:


     ```
     podman --runtime runsc-rootless runsc run --rm --rmi docker.io/library/debian:sid-slim dmesg | grep gVisor
     ```

2. Create volume with leaked Podman socket (`dangerous-podman-sock`):

   ```
   mkdir /path/to/socket/dir
   podman system service -t 0 unix:///path/to/socket/dir/podman.sock &
   podman volume create dangerous-podman-sock --opt device=/path/to/socket/dir --opt o=bind --opt type=bind
   ```

   It will be used by bot's control container to spawn hardened containers on the host.

   NOTE: group-writable permission on socket is needed because we start control container with internal user different than host user.

3. Provision Podman secrets for Debian Salsa integrations:

   * Visit Debian Salsa, generate token with "api" and "write_repository" scope and export it as `SALSA_TOKEN`
   * Export name of Podman socket volume as `PODMAN_SOCKET_VOLUME`
   * Run provisioning script from the directory holding this README file:

   ```
   export DEBIAN_SALSA=thesalsatoken
   export PODMAN_SOCKET_VOLUME=dangerous-podman-sock
   sh nd-bot-cli provision
   unset DEBIAN_SALSA
   unset PODMAN_SOCKET_VOLUME
   ```

4. Configure bot behavior by editing `deployment/control/bot-config.yaml`.
   The file is well-commented to understand the configurstion options.

5. Build and start bot instance:

   ```
   sh nd-bot-cli create <bot-instance-name>
   ```

Updating NeuroDebian Bot instance
--

1. Back the `deployment/control/bot-config.yaml` with custom settings up

2. Clone the repository again

3. Move the backed-up bot configuration file and edit it as needed

4. Perform update from the directory holding this README file:

   ```
   sh nd-bot-cli update <bot-instance-name>
   ```

Starting and stopping NeuroDebian Bot instance
--

To start an instance, run:


   ```
   sh nd-bot-cli start <bot-instance-name>
   ```
and to stop:


   ```
   sh nd-bot-cli stop <bot-instance-name>
   ```

Decommissioning NeuroDebian Bot instance
--

To destroy the instance without removing provisioned keys and result logs,
use:

   ```
   sh nd-bot-cli destroy <bot-instance-name>
   ```

To additionally remove keys from Debian Salsa, use:

   ```
   sh nd-bot-cli deprovision <bot-instance-name>
   ```

then log in to Debian Salsa using the favourite web browser and delete Persobal Access Token if needed.
