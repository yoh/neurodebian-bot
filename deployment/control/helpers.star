def builder_container_name(bot_name, distribution):
  return "{}-build-{}".format(bot_name, distribution)
end

def builder_image_name(bot_name, distribution):
  return "localhost/{}-build:{}".format(bot_name, distribution)
end

def buildspec_builder(bot_name, distribution, build_dir, tag):
  return "{} {} {}".format(builder_image_name(bot_name, distribution), build_dir, tag)
end

def buildspec_generic(bot_name, container_name, build_dir, tag):
  return "{} {} {}".format(generic_image_name(bot_name, container_name), build_dir, tag)
end

def downstream_build_name(downstream_branches, branch_idx, build_architectures, arch_idx, fallback):
  if arch_idx < 0:
    return fallback
  end
  return "BUILD-{}-{}".format(downstream_branches[branch_idx].packagingBranch.replace("/", "-").upper(), build_architectures[arch_idx].upper())
end

def downstream_import_name(downstream_branches, idx):
  return "REIMPORT-{}".format(downstream_branches[idx].packagingBranch.replace("/", "-").upper())
end

def generic_container_name(bot_name, container_name):
  return "{}-{}".format(bot_name, container_name)
end

def generic_image_name(bot_name, container_name):
  return "localhost/{}-{}:latest".format(bot_name, container_name)
end

def primary_build_name(primary_branch, build_architectures, arch_idx, fallback):
  if arch_idx < 0:
    return fallback
  end
  return "BUILD-{}-{}".format(primary_branch.packagingBranch.replace("/", "-").upper(), build_architectures[arch_idx].upper())
end

def primary_import_name(primary_branch):
  return "IMPORT-{}".format(primary_branch.packagingBranch.replace("/", "-").upper())
end

def primary_lint_name(primary_branch):
  return "LINT-{}".format(primary_branch.packagingBranch.replace("/", "-").upper())
end
